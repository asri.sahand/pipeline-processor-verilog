module muxALU0(input[15:0] imd,ReadData,input[1:0] ALU0,output[15:0] out);
  assign out=(ALU0==0)?ReadData:(ALU0==1)?imd:(ALU0==2)?65535:16'bz;
endmodule