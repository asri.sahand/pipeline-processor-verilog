module memToReg(input memToR,input[15:0] ALUout,memOut,output[15:0] out);
  assign out=(memToR==1)?memOut:ALUout;
endmodule