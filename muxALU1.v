module muxALU1(input[15:0] ReadR0Data,input[1:0] ALU1,output[15:0] out);
  assign out=(ALU1==0)?ReadR0Data:(ALU1==1)?16'b0:(ALU1==2)?65535:16'bz;
endmodule