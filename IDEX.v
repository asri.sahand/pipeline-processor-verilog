module IDEX(input clk,input[3:0] opc,input[1:0] WB,input M,input[13:0] EX,input[15:0] ReadData,ReadR0Data,imd,input[2:0] writeR,
	output reg[1:0] WBreg,output reg Mreg,output reg[13:0] EXreg,output reg[15:0] ReadDataReg,ReadR0DataReg,imdReg,output reg[2:0]writeRReg,output reg[3:0] opcReg); 
    initial begin 
        WBreg = 0; 
        Mreg = 0; 
        EXreg = 0; 
        ReadDataReg = 0; 
        ReadR0DataReg = 0; 
        imdReg = 0; 
        writeRReg=0;
    end 
     
    always@(posedge clk) 
    begin 
        opcReg <= opc;
        WBreg <= WB; 
        Mreg <= M; 
        EXreg <= EX; 
        ReadDataReg <= ReadData; 
        ReadR0DataReg <= ReadR0Data; 
        imdReg <= imd;
        writeRReg <= writeR;
    end 
     
endmodule