module ALU(input[15:0] a,b, input[8:0] func, output reg[15:0] out);
	wire[15:0] Clear, Moveto, Movefrom, Add, Sub, And, Or, Not;
	
	assign Clear = 0;
	assign Moveto = b;
	assign Movefrom = a;
	assign Add = a + b;
	assign Sub = b - a;
	assign And = a & b;
	assign Or = a | b;
	assign Not = ~a;
	
	always@(a or b) begin
	  out = func[0]? Clear : func[1]? Moveto : func[2]? Movefrom : func[3]? Add : func[4]? Sub : func[5]? And : func[6]? Or : func[7]? Not: out;
	end
	
	
endmodule