module ForwardUnit(input[3:0] opc,input writeRegWB,WriteReg,input[2:0] Ri,EMreg,MWreg,output reg[1:0] ALU0S,ALU1S, output reg stall);
   // Data Forwarding
   always@(EMreg, MWreg, Ri, WriteReg, writeRegWB)begin
    if((EMreg == Ri) && (WriteReg == 1))
      ALU0S = 2'b01;
    else if((MWreg == Ri) && (writeRegWB == 1))
     ALU0S = 2'b10;
    else
      ALU0S = 2'b00;
      
    if((EMreg == 3'b000) && (WriteReg == 1))
      ALU1S = 2'b01;
    else if((MWreg == 3'b000) && (writeRegWB == 1))
      ALU1S = 2'b10;
    else
      ALU1S = 2'b00;
    // LW
    /*if(opc == 4'b0000)begin
      if(writeRegWB==1'b1 && MWreg==3'b000)
        stall=1'b1;
      else
        stall=1'b0;
    end    */    
  end
endmodule