module muxRegWrite(input[2:0] Reg,input R0,output[2:0] R0out);
  assign R0out=R0?0:Reg;
endmodule