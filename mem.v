module mem(input[15:0] adr,writeData, input clk, write,output[15:0] out);
  reg[15:0] memory[0:2**12];
  initial $readmemb("data.txt", memory);
  assign out =memory[adr];
  always@(posedge clk) begin
	 if(write) begin
	 	 memory[adr] = writeData;
	 end
  end
endmodule