module all();
  wire Br,R0Write,MemToReg,writeMem,Wen,zero,IFIDWrite;
  wire[1:0] pcSel,ALU0,ALU1;
  wire[8:0] ALUop;
  wire[15:0] ins;
  wire stall;
  reg clk, rst;
  Controller c(stall,ins,zero,Br,R0Write,MemToReg,writeMem,Wen,IFIDWrite,pcSel,ALU0,ALU1,ALUop);
  DataPath dp(clk,Wen,IFIDWrite,R0Write,rst,Br,MemToReg,writeMem,pcSel,ALU0,ALU1,ALUop,zero,ins,stall);
  initial repeat(100) #49 clk = ~clk;
  initial begin
    clk = 0;
    rst = 1;
    #50 rst = 0;
  end
  

endmodule
