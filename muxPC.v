module muxPC(input[11:0] PC_plus1, insJmp, insBr, input[1:0] pc_sel, output[11:0] out);
	assign out = (pc_sel==0)? PC_plus1:(pc_sel==1)?insJmp:(pc_sel==2)?insBr:12'bz;
endmodule