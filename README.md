# Pipeline processor	

This is a pipeline processor project in verilog language

## Instruction Set

The instruction set is available in instruction-set.pdf 

## Tests

Test cases are available in data.txt

### Test instructions

You can test all of the processor's instructions with data.txt

## Built With

* [ModelSim](https://www.mentor.com/company/higher_ed/modelsim-student-edition) - The simulation environment used
* [Verilog](https://en.wikipedia.org/wiki/Verilog) - The Hardware Description Language used

## Authors

* **Sahand Asri**
* **Sasan Yasari**