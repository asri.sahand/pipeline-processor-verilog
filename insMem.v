module insMem(input[11:0] adr,output[15:0] ins);
  reg[15:0] memory[0:2**12];
 	initial $readmemb("instructions1.txt",memory);
  assign ins = memory[adr];
endmodule