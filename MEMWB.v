module MEMWB(input clk,input[3:0] opc,input[1:0] WB,input[15:0] Memout,ALUOut,input[2:0] writeR,
			 output reg[1:0] WBReg,output reg[15:0] Memreg,ALUreg,output reg[2:0]writeRReg, output reg[3:0] opcReg); 
   initial begin 
      WBReg = 0; 
      Memreg = 0; 
      ALUreg = 0; 
      writeRReg = 0; 
   end 
    
    always@(posedge clk) 
    begin 
        opcReg <= opc;
        WBReg <= WB; 
        Memreg <= Memout; 
        ALUreg <= ALUOut; 
        writeRReg <= writeR; 
    end 
    
endmodule