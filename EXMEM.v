module EXMEM(input clk,input[3:0] opc,input[1:0] WB,input M,input[15:0] adr,WriteDataIn,ALUOut,input[2:0] writeR,
			 output reg[1:0] WBreg,output reg Mreg,output reg[15:0] adrReg,WriteDataOut,ALUReg,output reg[2:0] writeRReg,output reg[3:0] opcReg); 
   initial begin 
      WBreg=0; 
      Mreg=0; 
      ALUReg=0; 
      adrReg=0;
      WriteDataOut=0; 
      writeRReg=0;
   end 
    
    
    always@(posedge clk) 
    begin 
        opcReg <= opc;
        WBreg <= WB; 
        Mreg <= M; 
        ALUReg <= ALUOut; 
        WriteDataOut <= WriteDataIn; 
        writeRReg <= writeR;
        adrReg <= adr;
    end 
 
endmodule