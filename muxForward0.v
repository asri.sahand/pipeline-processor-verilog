module muxForward0(input[15:0] readData,ALUout,Mem,input[1:0] Sel,output[15:0] out);
  assign out=(Sel==0)?readData:(Sel==1)?ALUout:(Sel==2)?Mem:16'bz;
endmodule