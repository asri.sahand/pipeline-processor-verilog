module pc(input[11:0] in,input Br,clk,rst,output reg[11:0] out);
  always@(posedge clk)begin 
    if(rst)
      out=0;
    else if(Br)
      out={out[11:9],in[8:0]};
    else 
      out=in;
  end
  //always @(posedge clk) $display("pc : %b", out);
endmodule