module regFile(input Wen,clk,input[2:0] regRead,regWrite,input[15:0] writeData,output [15:0] readData,readR0Data);
  reg [15:0] regs[0:7];
  assign readData=regs[regRead];
  assign readR0Data=regs[0];
  always@(posedge clk)begin
    if(Wen)
      regs[regWrite]=writeData;
  end
endmodule