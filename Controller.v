module Controller(input stall,input[15:0] ins,input zero,output reg Br,R0Write,MemToReg,writeMem,Wen,IFIDWrite,
                  output reg[1:0] pcSel, ALU0,ALU1, output reg[8:0] ALUop);
  always@(*)begin
    Br = 0; R0Write = 0; MemToReg = 0; writeMem = 0; Wen = 0;
    pcSel = 2'b00; ALU0=2'b00; ALU1=2'b00;IFIDWrite=1;
    ALUop = 0;
    if(stall==1'b1)begin IFIDWrite=0; end
    case (ins[15:12])
      4'b0000: begin R0Write = 1; MemToReg=1; Wen = 1; end
      4'b0001: begin writeMem = 1; end
      4'b0010: begin pcSel = 2'b01; end
      4'b0100: begin if(zero)begin pcSel = 2'b10; Br = 1; end end
      4'b1000: begin
        case (ins[8:0])
          9'b000000001: begin ALUop=ins[8:0]; Wen=1; ALU1=2'b01; end
          9'b000000010: begin ALUop=ins[8:0]; Wen=1; R0Write=0; ALU0=2'b10; end
          9'b000000100: begin ALUop=ins[8:0]; Wen=1; R0Write=1; ALU1=2'b10; end
          9'b000001000: begin ALUop=ins[8:0]; Wen=1; R0Write=1; end
          9'b000010000: begin ALUop=ins[8:0]; Wen=1; R0Write=1; end
          9'b000100000: begin ALUop=ins[8:0]; Wen=1; R0Write=1; end
          9'b001000000: begin ALUop=ins[8:0]; Wen=1; R0Write=1; end
          9'b010000000: begin ALUop=ins[8:0]; Wen=1; R0Write=1; end
          9'b100000000: begin ALUop=ins[8:0]; Wen=0; R0Write=0; IFIDWrite=1; end
        endcase      
      end
      4'b1100: begin ALU0=1; R0Write=1; Wen=1; ALUop=9'b000001000; end
      4'b1101: begin ALU0=1; R0Write=1; Wen=1; ALUop=9'b000010000; end
      4'b1110: begin ALU0=1; R0Write=1; Wen=1; ALUop=9'b000100000; end
      4'b1111: begin ALU0=1; R0Write=1; Wen=1; ALUop=9'b001000000; end
    endcase
  end

endmodule