module IFID(input clk,input[3:0] opc,input IFIDWrite,input[11:0]PC,input[15:0] ins,output reg[15:0]insReg,
  output reg[11:0]PCReg, output reg[3:0] opcReg); 
    initial begin 
        insReg = 0; 
        PCReg = 0; 
    end 
    always@(negedge clk) 
    begin 
        if(IFIDWrite) 
        begin 
           opcReg <= opc;
           insReg <= ins; 
           PCReg <= PC; 
        end 
    end 
 
endmodule